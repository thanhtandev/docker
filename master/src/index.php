<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker Tutorial</title>
</head>
<body>
    <h1>
        Hello World
    </h1>
    <h2>
        Welcome to Docker Tutorial
    </h2>
    <h3>
        <?php echo 'We are running PHP, version: ' . phpversion(); ?>
    </h3>
    <p>
        Docker is an open platform for developing, shipping, and running applications. 
        Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 
        With Docker, you can manage your infrastructure in the same ways you manage your applications. 
        By taking advantage of Docker's methodologies for shipping, testing, 
        and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.
    </p>
    <?  
        $database ="mydb";  
        $user = "root";  
        $password = "secret";
        $host = "mysql";  

        $connection = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $password);  
        $query = $connection->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_TYPE='BASE TABLE'");  
        $tables = $query->fetchAll(PDO::FETCH_COLUMN);  

        if (empty($tables)) {
          echo "<p>There are no tables in database \"{$database}\".</p>";
        } else {
          echo "<p>Database \"{$database}\" has the following tables:</p>";
          echo "<ul>";
            foreach ($tables as $table) {
              echo "<li>{$table}</li>";
            }
          echo "</ul>";
        }
    ?>
</body>
</html>